#! /usr/bin/env python3

import argparse
import datetime
import flockfile
import logging
import os
import re
import shutil
import subprocess
import yaml

from pathlib import Path

class Onboarder:

    def __init__(self, config_file):

        # read the config file
        if not os.path.isfile(config_file):
            raise FileNotFoundError("Config file not found")
        with open(config_file, "r") as f:
            config = yaml.safe_load(f)

        # init the logger
        log_file_fmt = config.get("log_file_fmt", None)
        if log_file_fmt:
            log_file = datetime.datetime.now.strftime(log_file_fmt)
            os.makedirs(os.path.dirname(log_file), exist_ok=True)
            logging.basicConfig(level=config.get('loglevel', 'INFO'), filename=log_file)
        else:
            logging.basicConfig(level=config.get('loglevel', 'INFO'))
        self.log = logging.getLogger()

        # who owns it?
        self.chown_user = config.get("chown_user", None)
        self.log.debug("Will attempt to make files owned by user {}".format(self.chown_user))

        # ffmpeg - make sure we have the right executable
        self.ffmpeg_exe = config.get("ffmpeg_exe", shutil.which("ffmpeg"))
        if not self.ffmpeg_exe:
            self.log.error("ffmpeg executable not found in path")
            raise FileNotFoundError("ffmpeg executable not found in path")
        elif not os.path.isfile(self.ffmpeg_exe):
            self.log.error("ffmpeg executable not found at {}".format(self.ffmpeg_exe))
            raise FileNotFoundError("ffmpeg executable not found.")
        else:
            self.log.debug("Using ffmpeg executable at {}".format(self.ffmpeg_exe))

        # ffprobe - make sure we have the right executable
        self.ffprobe_exe = config.get("ffprobe_exe", shutil.which("ffprobe"))
        if not self.ffprobe_exe:
            self.log.error("ffprobe executable not found in path")
            raise FileNotFoundError("ffprobe executable not found in path")
        elif not os.path.isfile(self.ffprobe_exe):
            self.log.error("ffprobe executable not found at {}".format(self.ffprobe_exe))
            raise FileNotFoundError("ffprobe executable not found.")
        else:
            self.log.debug("Using ffprobe executable at {}".format(self.ffprobe_exe))

        self.source_file_exts = config.get("source_file_exts")
        self.log.debug("Finding files with extensions: {}".format(", ".join(self.source_file_exts)))

        self.source_dir = config.get("source_dir")
        self.log.debug("Looking in source directory: {}".format(self.source_dir))

        self.dest_dir = config.get("dest_dir")
        self.log.debug("Destination root directory: {}".format(self.dest_dir))

        self.dest_subdir_patterns = config.get("dest_subdir_patterns")
        self.log.debug("Destination subdir patterns: {}".format(self.dest_subdir_patterns))

        self.process_threads = config.get("process_threads", 2)

    def run(self, dry_run=True):

        # find all files with the right extensions
        source_files = filter(
            lambda x: x.suffix.strip(".").lower() in self.source_file_exts,
            Path(self.source_dir).rglob('*'),
        )

        # pattern for pulling info from the dashcam files
        pattern = re.compile(r"\w+_(\d+_\d+)([AB])(SOS)?\.(\w+)")

        for i in source_files:

            ifilename = os.path.split(str(i))[1]
            ofilename = "{}.mkv".format(os.path.splitext(ifilename)[0])
            idir = os.path.dirname(str(i))

            # make sure filename matches a regex pattern
            match = re.match(pattern, ifilename)
            if not match:
                self.log.info("File does not match pattern: {}".format(ifilename))
                continue

            self.log.info("Processing File: {}".format(ifilename))
            file_parts = match.groups()
            file_date = datetime.datetime.strptime(file_parts[0], "%Y%m%d_%H%M%S")
            
            part_to_cam = {
                "A": "front",
                "B": "rear",
            }
            cam_type = part_to_cam.get(file_parts[1], "other")

            morn_evening = {
                "AM": "M",
                "PM": "E",
            }
            morning_evening = morn_evening.get(file_date.strftime("%p").upper())

            new_file_subdirs = list(map(
                lambda x: x.format(
                    year=file_date.strftime("%y"),
                    month=file_date.strftime("%m"),
                    day=file_date.strftime("%d"),
                    day_inital=file_date.strftime("%a")[0],
                    morning_evening=morning_evening,
                    camera_type=cam_type,
                ),
                self.dest_subdir_patterns,
            ))

            new_dest_dir = self.dest_dir
            for sd in new_file_subdirs:
                new_dest_dir = os.path.join(new_dest_dir, sd)
                if not dry_run:
                    if not os.path.isdir(new_dest_dir):
                        os.makedirs(new_dest_dir, exist_ok=True)
                        if self.chown_user:
                            shutil.chown(new_dest_dir, user=self.chown_user, group=self.chown_user)

            dest_file = os.path.join(
                new_dest_dir,
                ofilename,
            )

            self.log.debug("File will be moved to: {}".format(dest_file))

            cmd = [
                self.ffmpeg_exe,
                "-threads", str(self.process_threads),
                "-y", "-v", "error",
                "-i", ifilename,
                "-c:v", "libx264",
                "-preset", "slow",
                "-crf", "28",
                "-c:a", "mp3",
                ofilename,
            ]
            self.log.debug("ffmpeg command: {}".format(" ".join(cmd)))
            self.log.debug("ffmpeg will be run from dir: {}".format(idir))

            if not dry_run:
                # run ffmpeg
                ffproc = subprocess.run(cmd, capture_output=True, cwd=idir)
                if ffproc.returncode != 0:
                    # if it crashes, log the error, remove the temp file
                    self.log.error("ffmpeg failed{ls}stdout:{so}{ls}stderr:{se}".format(
                        ls=os.linesep,
                        so=ffproc.stdout,
                        se=ffproc.stderr,
                    ))
                    os.remove(os.path.join(idir, ofilename))
                else:
                    # if it succeeds, chown, move, remove source
                    self.log.debug("Moving temp file to destination, removing source.")
                    shutil.move(os.path.join(idir, ofilename), dest_file)
                    shutil.chown(dest_file, user=self.chown_user, group=self.chown_user)
                    os.remove(str(i))
                    self.log.info("Conversion was successful")

if __name__ == '__main__':

    lock = flockfile.FlockFile("onboard-video")
    lock.lock()

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config", dest="config", default="/etc/onboard-video-config.yml",
        help="Path to the config file.",    
    )

    parser.add_argument(
        "--dry-run", dest="dry_run", default=False, action="store_true",
        help="dry run",
    )
    args = parser.parse_args()

    ob = Onboarder(args.config)
    ob.run(dry_run=args.dry_run)

    lock.unlock()
