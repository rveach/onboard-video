from distutils.core import setup
setup(
  name = 'onboard-video',
  packages = [],
  version = '0.1.3',
  description = 'onboard video files',
  author = 'ryan',
  author_email = 'ryan@example.com',
  url = 'https://gitlab.com/rveach/onboard-video',
  download_url = 'https://gitlab.com/rveach/onboard-video/-/archive/master/onboard-video-master.tar.gz',
  keywords = ['ffmpeg'], # arbitrary keywords
  classifiers = [],
  install_requires=[
    'flockfile>=0.1.2',
    'pyyaml>=5.2',
  ],
  scripts=['onboard-video.py'],
)
